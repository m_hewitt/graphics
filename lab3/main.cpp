// Authors: Michael Hewitt, Grace Walkuski
// CSCI 441
// 02-13-2018

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <csci441/shader.h>

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

int main(void) {
    /* Initialize the library */
    GLFWwindow* window;
    if (!glfwInit()) {
        return -1;
    }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Lab 3", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the triangle drawing */
    // define the vertex coordinates of the triangle
    float triangle[] = {
         0.5f,  0.5f, 1.0, 0.0, 0.0,
         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,

         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f, -0.5f, 0.0, 0.0, 1.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,
    };

    // create and bind the vertex buffer object and copy the data to the buffer
    GLuint VBO[1];
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    // create and bind the vertex array object and describe data layout
    GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 5*sizeof(float), (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("vert.glsl", "frag.glsl");

    //initialize tranformation matrix
    float m[] = {
      1,0,0,
      0,1,0,
      0,0,1
    };

    //rendering mode
    int mode=0;

    // whether space bar is held down
    bool pressed = false;

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        glfwSetInputMode(window, GLFW_STICKY_KEYS, 0);
        // process input
        processInput(window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // use the shader
        shader.use();
        /** Part 2 animate and scene by updating the transformation matrix */
        float timeValue = glfwGetTime();
        float t = (sin(timeValue));

        //scale
        if (mode == 3){
          for (int i=0;i<9;i++){
            if(i%4 == 0){
              m[i]=t;
            }
            else{
              m[i]=0;
            }
          }
        }

        //rotate center
        else if (mode == 1){
          m[0]=cos(t);
          m[1]=-sin(t);
          m[2]=0;
          m[3]=sin(t);
          m[4]=cos(t);
          m[5]=0;
          m[6]=0;
          m[7]=0;
          m[8]=1;
        }

        //translate
        else if (mode == 2){
          m[0]=1;
          m[1]=0;
          m[2]=0;
          m[3]=0;
          m[4]=1;
          m[5]=0;
          m[6]=t;
          m[7]=t;
          m[8]=1;
        }

        //rotate off center
        else{
          m[0]=cos(t);
          m[1]=-sin(t);
          m[2]=0;
          m[3]=sin(t);
          m[4]=cos(t);
          m[5]=0;
          m[6]=(-.5*cos(t))-(.5*sin(t))+.5;
          m[7]=(-.5*cos(t))+(.5*sin(t))+.5;
          m[8]=1;
        }

        // send transformation matrix to vertex shader
        int vertexLocation = glGetUniformLocation(shader.id(), "m");
        glUniformMatrix3fv(vertexLocation, 1, GL_FALSE, m);

        // draw our triangles
        glBindVertexArray(VAO[0]);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(triangle));

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);

        /* Poll for and * process * events */
        glfwPollEvents();

        //handle spacebar input (only get one input per press)
        if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS && pressed == false){
          mode+=1;
          pressed = true;
        }
        if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE && pressed == true){
          pressed = false;
        }

        //cycle through modes
        if(mode>=4){
          mode=0;
        }
    }

    glfwTerminate();
    return 0;
}
