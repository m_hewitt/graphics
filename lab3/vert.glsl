#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec3 aColor;
uniform mat3 m;

out vec3 myColor;

void main() {

    vec3 bPos = vec3(aPos, 1.0);
    bPos = m * bPos;
    gl_Position = vec4(bPos, 1.0);
    myColor = aColor;
}
