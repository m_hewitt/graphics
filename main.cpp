#include <iostream>
#include <string>



class Vector3 {
public:
    float x;
    float y;
    float z;

    // Constructor
    Vector3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {
        // nothing to do here as we've already initialized x, y, and z above
        std::cout << "in Vector3 constructor" << std::endl;
    }
    Vector3() : x(0), y(0), z(0){

    }

    // Destructor - called when an object goes out of scope or is destroyed
    ~Vector3() {
        // this is where you would release resources such as memory or file descriptors
        // in this case we don't need to do anything
        std::cout << "in Vector3 destructor" << std::endl;
    }
};

Vector3 operator+(Vector3& v, Vector3& v2) { // v and v2 are copies, so any changes to them in this function
                                     // won't affect the originals
    float x3 = v.x + v2.x;
    float y3 = v.y + v2.y;
    float z3 = v.z + v2.z;
    Vector3 v3(x3,y3,z3);
    return v3;
}
std::ostream& operator<<(std::ostream& stream, const Vector3& v) {
    // std::cout is a std::ostream object, just like stream
    // so use stream as if it were cout and output the components of
    // the vector
    stream << v.x << ", " << v.y << ", " << v.z;
    return stream;
}
int main(int argc, char** argv){
  std::cout << "What is your name?" << std::endl;
  std::string name;
  std::cin >> name;
  std::cout << "Hello " << name << std::endl;
  Vector3 a(1,2,3);   // allocated to the stack
  Vector3 b(4,5,6);
                          // copies 6 floats to the arguments in add (3 per Vector3),
                          // 3 more floats copied into c when it returns
                          // a and b are unchanged
  std::cout << a + b << std::endl;
  Vector3* d = new Vector3(0,0,0);
  d -> y = 5;
  std::cout << *d << std::endl;
  Vector3* array = new Vector3[10];
  for (int i = 0; i < 10; i++){
    array[i].y=5;
    std::cout << array[i] << std::endl;
  }
}
