#ifndef _CSCI441_SHAPE_H_
#define _CSCI441_SHAPE_H_

#include <cstdlib>
#include <vector>

//points of a freeform polygon
struct point{
  point *next;
  point *prev;
  float x;
  float y;
};


template <typename T, typename N, typename C>
void add_vertex(T& coords, const N& x, const N& y,
        const C& r, const C& g, const C& b, bool with_noise=false) {
    // adding color noise makes it easier to see before shading is implemented
    float noise = 1-with_noise*(rand()%150)/100.;
    coords.push_back(x);
    coords.push_back(y);
    coords.push_back(r*noise);
    coords.push_back(g*noise);
    coords.push_back(b*noise);
}

//Does Triangle ABC contain point p?
bool contains(float Ax, float Ay, float Bx, float By, float Cx, float Cy, float Px, float Py){
  Vector a(Ax, Ay);
  Vector b(Bx, By);
  Vector c(Cx, Cy);
  Vector p(Px, Py);
  Vector gamma = b - a;
  Vector beta = c - a;
  Vector alpha = p - a;
  float gg = gamma * gamma;
  float gb = gamma * beta;
  float bb = beta * beta;
  float ga = gamma * alpha;
  float ba = beta * alpha;
  float denom = (gg * bb) - (gb * gb);
  float l1 = ((bb * ga) - (gb * ba)) / denom;
  float l2 = ((gg * ba) - (gb * ga)) / denom;
  float l3 = 1.0f - l1 - l2;
  if (l1 >= 0 && l2 >= 0 && l3 >= 0 && l1 <= 1 && l2 <= 1 && l3 <= 1){
    return true;
  }
  else{
    return false;
  }
}


template <typename T>
Vector center(T& coords){
  //mean of all vertices
  int count = 0;
  float ytot = 0;
  float xtot = 0;
  for (int i = 0; i < coords.size(); i+=5){
    xtot += coords[i];
    ytot += coords[i+1];
    count += 1;
  }

  Vector v (xtot/count, ytot/count);
  return (v);
}

class Square {
public:
    std::vector<float>coords;
    Square() : coords {
      0.5f,  0.5f, 0.0, 0.0, 0.0,
      0.5f, -0.5f, 0.0, 0.0, 0.0,
     -0.5f,  0.5f, 0.0, 0.0, 0.0,

      0.5f, -0.5f, 0.0, 0.0, 0.0,
     -0.5f, -0.5f, 0.0, 0.0, 0.0,
     -0.5f,  0.5f, 0.0, 0.0, 0.0,
    }{}
};
class Triangle {
public:
  std::vector<float> coords;
  Triangle () : coords{
    -0.5, -0.5, 0.0, 0.0, 0.0,
    0.5, -0.5, 0.0, 0.0, 0.0,
    0, 0.5, 0.0, 0.0, 0.0
  }{}
};
class Circle {
public:
  std::vector<float> coords;
  Circle(unsigned int n, float r, float g, float b) {
      double step_size = 2*M_PI / n;
      double c_x=0;
      double c_y=0;
      float radius = .5;

      for (int i = 0; i <= n + 1; ++i) {
          // vertex i
          double theta_i = i*step_size;
          double vi_x = radius*cos(theta_i);
          double vi_y = radius*sin(theta_i);

          // vertex i+1
          double theta_ip1 = ((i+1)%n)*step_size;
          double vip1_x = radius*cos(theta_ip1);
          double vip1_y = radius*sin(theta_ip1);

          // add triangle viL, viH, vip1L
          add_vertex(coords, vip1_x, vip1_y, r, g, b);
          add_vertex(coords, vi_x, vi_y, r, g, b);
          add_vertex(coords, c_x, c_y, r, g, b);
      }
  }
};
class Poly {
  //doubly, circular, linked list of x,y vertices
public:
  point *first;
  point *current;
  Poly(){
    first = new point;
    first->next = 0;
    first->x = 1000;
  }
  void addPoint(float x, float y){
    if (first->x == 1000){
      first->x = x;
      first->y = y;
      current = first;
    }
    else{
      point *n;
      n = new point;
      n->x = x;
      n->y = y;
      current->next = n;
      n->prev = current;
      current = n;
    }

  }
  void clear(){
    first->next = 0;
    first->x = 1000;
  }

};
class PolyShape {
public:
  std::vector<float> coords;
  point *conductor;
  point *first;
  point *last;
  PolyShape(Poly p, float r, float g, float b){
    first = p.first;
    last = p.current;
    first->prev = last;
    last->next = first;

    point *left;
    bool captures = false;


    while (size() > 2){
      //while there are enough vertices to make a triangle
      captures = true;
      conductor = first->next;
      left = first;
      while(conductor != first){
        //find leftmost point
          if (conductor -> x < left -> x){
            left = conductor;
          }
          conductor = conductor->next;
        }
        while(captures){
          captures = false;
          conductor = left->next->next;
          while(conductor != left->prev){
            //dont draw triangle if it contains another point in it, move to next vertex
            if (contains(left->x, left->y, left->next->x, left->next->y, left->prev->x, left->prev->y, conductor->x, conductor->y)){
              captures = true;
              left = left->next;
              break;
            }
            conductor = conductor->next;
          }

        }
        //draw triangle
        add_vertex(coords, left->x, left->y, r, g, b);
        add_vertex(coords, left->next->x, left->next->y, r, g, b);
        add_vertex(coords, left->prev->x, left->prev->y, r, g, b);
        first = left->next;
        //remove ear tip
        rem(left);
      }
  }
  void rem(point *tip){
    tip->prev->next = tip->next;
    tip->next->prev = tip->prev;
  }
  int size(){
    int count = 1;
    conductor = first->next;
      while(conductor != first){
        count += 1;
        conductor = conductor->next;
      }
    return (count);
  }
};
class Combo{
public:
  std::vector<float> coords;
  template <typename T>
  Combo(T& points){
    for (int i = 0; i < points.size(); i++){
      coords.push_back(points[i]);
    }
  }
};

#endif
