#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include "matrix.h" //edited for 2D
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h" //edited for 2D


const int SCREEN_WIDTH = 900;
const int SCREEN_HEIGHT = 700;

//allow mode functions to handle click events
bool click = false;

//click locations (used for initial position of drag)
double clickx;
double clicky;

//left or right click
//TODO: change variable name
int prim = 0;
Matrix* view;
Matrix camera;

//shape for free form
Poly p;


//Linked List Nodes for holding shapes
struct node {
  int index;
  float mem;
  Matrix translate;
  Matrix transform;
  bool selected;
  std::vector<float> points;
  node *next;
};
node *root;
node *last;

//count number of combination shapes that exist
int combo = 1;



void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix processModel(const Matrix& model, GLFWwindow *window) {
    Matrix trans;

    const float ROT = 1;
    const float SCALE = .05;

    // ROTATE[]
    if (isPressed(window, '[')) {
      trans.rotate_z(-ROT);
    }
    else if (isPressed(window, ']')) {
      trans.rotate_z(ROT);
    }
    // SCALE
    if (isPressed(window, GLFW_KEY_Z)) {
      trans.scale(1-SCALE, 1);
    }
    else if (isPressed(window, GLFW_KEY_X)) { trans.scale(1+SCALE, 1); }
    else if (isPressed(window, GLFW_KEY_C)) { trans.scale(1, 1+SCALE); }
    else if (isPressed(window, GLFW_KEY_V)) { trans.scale(1, 1-SCALE); }
    // TRANSLATE


    return trans * model;
}
Matrix processCamera(const Matrix& camera, GLFWwindow *window) {
    Matrix trans;
    const float TRANS = .01;

    if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0, -TRANS); }
    else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0, TRANS); }
    else if (isPressed(window, GLFW_KEY_LEFT)) { trans.translate(TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.translate(-TRANS, 0); }
    return trans * camera;
  }

void cameraMovement(Matrix& camera, GLFWwindow *window) {
  camera = processCamera(camera, window);
}

void processInput(Matrix& model, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
    const float TRANS = .01;
    Matrix trans;
    Matrix temp;
    if (isPressed(window, GLFW_KEY_W)) { trans.translate(0, TRANS); }
    else if (isPressed(window, GLFW_KEY_S)) { trans.translate(0, -TRANS); }
    else if (isPressed(window, GLFW_KEY_A)) { trans.translate(-TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_D)) { trans.translate(TRANS, 0); }
    else {
      temp = processModel(model, window);
    }
    node *n;

    n = root->next;
    while (n != 0){
      if (n->selected){
        //every shape node has its own transformations
        n->translate = trans * n->translate;
        n->transform = temp * n->transform;
      }
      n = n->next;
    }
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

void dragEvent(GLFWwindow* window){
  //translate shape by the distance of a drag
  node *conductor;
  Matrix trans;
  int width, height;
  glfwGetFramebufferSize(window, &width, &height);
  double mousex;
  double mousey;
  glfwGetCursorPos(window, &mousex, &mousey);

  mousey = -1 * ((mousey*2 / ((float)height/2)) - 1);
  mousex = 1 * ((mousex*2 / ((float)width/2)) - 1);
  clickx = 1 * ((clickx*2 / ((float)width/2)) - 1);
  clicky = -1 * ((clicky*2 / ((float)height/2)) - 1);

  Vector v(clickx, clicky);
  Vector end(mousex, mousey);

  trans.translate(end.x() - v.x(), end.y() - v.y());
  end =  camera * conductor -> transform.inverse() * conductor->translate * view->inverse() * end;
  conductor = root->next;
  while (conductor!=0){
    v.setxy(clickx, clicky);
    v =  camera * conductor -> transform.inverse() * conductor->translate * view->inverse() * v;
    for (int i = 0; i < conductor->points.size() - 10; i+=15){
      if (contains(conductor->points[i],conductor->points[i+1],conductor->points[i+5],conductor->points[i+6],conductor->points[i+10],conductor->points[i+11],v.x(), v.y())){
          conductor->translate = trans * conductor->translate;
        }
      }
      conductor = conductor->next;
    }
}
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
      if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS && !click){
        prim = 1;
        click = true;
        glfwGetCursorPos(window, &clickx, &clicky);
      }
      if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS && !click){
        prim = 0;
        click = true;
      }
      if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE){
        dragEvent(window);
      }
}
template <typename N>
void addNode(N& s){
//add a shape s to linked list of shapes
  Vector c = center(s.coords);
  for (int i = 0; i < s.coords.size(); i += 5){
    //put shape at origin
    s.coords[i] = s.coords[i] - c.x();
    s.coords[i + 1] = s.coords[i + 1] - c.y();
  }
  Matrix trans;
  node* n;
  n = new node;
  n -> next = 0;
  Matrix m;
  trans.translate(c.x(), c.y());
  n -> translate = trans;
  n -> transform = m;
  n -> selected = true;
  n -> index = 0;
  n -> mem = s.coords.size()*sizeof(float);
  n -> points = s.coords;
  //draw the shape, but will not be permanent until n=last
  last -> next = n;

}
void color_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    //control RGB
    if (key == 49 || key == 50 || key == 51){
      node *conductor;
      int mod = 1;
      if (mods == GLFW_MOD_SHIFT){
        mod = -1;
      }
    conductor = root->next;
    while (conductor != 0){
        if (conductor->selected){
          for (int i = key - 47; i < conductor->points.size(); i+=5){
            if(conductor->points[i]>1){
              conductor->points[i] = 1;
            }
            else if(conductor->points[i]<0){
              conductor->points[i] = 0;
            }
            else{
              conductor->points[i]+=.05 * mod;
            }
          }
        }
        conductor = conductor->next;
      }
  }
}

void mode2 (GLFWwindow* window) {
  //when in mode 2
  node* conductor;
  if (isPressed(window,GLFW_KEY_ENTER)){
    //separate grouped shape
    node* stepper;
    conductor = root->next;
    while(conductor != 0){
      if (conductor->selected){
        if(conductor->index<0){
          stepper = root->next;
          while(stepper != 0){
            if(stepper->index == conductor->index){
              //stepper is in the same group as conductor
              Vector c = center(stepper->points);
              Vector temp (c.x(), c.y());
              temp = stepper->translate.inverse() * temp;

              for (int i = 0; i < stepper->points.size(); i += 5){
                stepper->points[i] = stepper->points[i] - c.x();
                stepper->points[i + 1] = stepper->points[i+1] - c.y();
              }
              Matrix trans;
              Matrix t;
              trans.translate(c.x(),c.y());
              stepper->translate = stepper->transform * trans * stepper->translate;
              stepper->transform = t;
            }
            stepper = stepper->next;
          }
          //remove all selected groups
          conductor->index = 0;
          combo -= 1;
        }
      }
      conductor = conductor->next;
    }
  }
  if(click){
    //when there is a mouse click
    int width, height;
    node *stepper;
    glfwGetFramebufferSize(window, &width, &height);
    double mousex;
    double mousey;
    glfwGetCursorPos(window, &mousex, &mousey);
    mousey = -1 * ((mousey*2 / ((float)height/2)) - 1);
    mousex = 1 * ((mousex*2 / ((float)width/2)) - 1);

    conductor = root->next;
    Vector v(mousex,mousey);
    std::vector<float> points;
    while (conductor!=0){
      //get current world coordinates for mouse coordinates
      v.setxy(mousex, mousey);
      v =   camera * conductor ->transform.inverse() * conductor->translate * view->inverse() * v;
      for (int i = 0; i < conductor->points.size() - 10; i+=15){
        if (contains(conductor->points[i],conductor->points[i+1],conductor->points[i+5],conductor->points[i+6],conductor->points[i+10],conductor->points[i+11],v.x(), v.y())){
          if(prim == 1){
            //left click
            if (conductor->selected){
              //switch select state
              conductor->selected = false;
              if (conductor->index < 0){
                stepper = root->next;
                while(stepper != 0){
                  if(stepper->index == conductor->index){
                    //select all shapes in a grouping
                    stepper->selected = false;
                  }
                  stepper = stepper->next;
              }
            }
          }
            else{
              //same as above
              conductor->selected = true;
              if (conductor->index < 0){
                stepper = root->next;
                while(stepper != 0){
                  if(stepper->index == conductor->index){
                    stepper->selected = true;
                  }
                  stepper = stepper->next;

              }
            }
          }
            break;
          }

        }

      }
      if(prim == 0){
        //right click
        if (conductor->selected){
          for(int i = 0; i < conductor->points.size(); i+=5){
            //make a sudo shape that will not be drawn(used to find center of group)
            conductor->index = -combo;
            Vector temp (conductor->points[i], conductor->points[i+1]);
            temp =  conductor->translate * conductor->transform *temp;
            points.push_back (temp.x());
            points.push_back (temp.y());
            points.push_back (conductor->points[i+2]);
            points.push_back (conductor->points[i+3]);
            points.push_back (conductor->points[i+4]);
          }
        }
      }
      conductor = conductor->next;
    }
    if(prim == 0){
      //right click
      Combo s(points);

      conductor = root;
      while (conductor != 0){
        if(conductor->selected){
          Vector c = center(s.coords);
          //center of group
          for (int i = 0; i < conductor->points.size(); i += 5){
            //convert transformations to CPU side, sudo origin at group center
            Vector temp (conductor->points[i], conductor->points[i+1]);
            temp =  conductor->translate * conductor->transform *temp;
            conductor->points[i] = temp.x() - c.x();
            conductor->points[i + 1] = temp.y() - c.y();
          }
          Matrix trans;
          Matrix t;
          trans.translate(c.x(),c.y());
          conductor->translate = trans;
          conductor->transform = t;
          combo += 1;
        }
        conductor = conductor->next;
      }
    }

  }
}
void mode1(GLFWwindow* window){
  //when in mode 1
    if (isPressed(window, GLFW_KEY_7)){
      Circle s(20, 0, 0, 0);
      addNode(s);
    }
    else if (isPressed(window, GLFW_KEY_8)){
      Square s;
      addNode(s);
    }
    else if (isPressed(window, GLFW_KEY_9)){
      Triangle s;
      addNode(s);
    }
}
void mode0(GLFWwindow* window){
  //when in mode 0
  if (click){
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    double mousex;
    double mousey;
    glfwGetCursorPos(window, &mousex, &mousey);
    mousey = -1 * ((mousey*2 / ((float)height/2)) - 1);
    mousex = 1 * ((mousex*2 / ((float)width/2)) - 1);
    //right click
    if (prim == 0){
      if (p.first->x != 1000){
        //form shape out of polygon points
        PolyShape s(p, 0, 0, 0);
        addNode(s);
        p.clear();
      }
    }
    //left click
    else if (prim == 1){
      //add point to polygon
      p.addPoint(mousex,mousey);
    }

  }
}


int main(void) {

    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "lab5", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    //initialize linked list
    node *iter;
    root = new node;
    root -> mem = 0;
    root -> next = 0;
    last = root;

    GLuint VBO1;
    glGenBuffers(1, &VBO1);
    glBindBuffer(GL_ARRAY_BUFFER, VBO1);
    glBufferData(GL_ARRAY_BUFFER, 1, NULL, GL_STATIC_DRAW);

    // describe vertex layout
    GLuint VAO1;
    glGenVertexArrays(1, &VAO1);
    glBindVertexArray(VAO1);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5*sizeof(float),
            (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);



    Shader shader("vert.glsl", "frag.glsl");

    // setup the textures
    shader.use();

    // set the matrices

    Matrix model;

    bool spacePressed = false;
    view = new Matrix();
    int mode = 0;
    int width, height;
    float max_mem = 0;
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        glLineWidth(100.0);
        processInput(model, window);
        /* Render here */
        cameraMovement(camera, window);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        // activate shader

        shader.use();

        glfwGetFramebufferSize(window, &width, &height);
        double newarx = ((float)width / (float)height);

        view->ortho(-newarx, newarx, -1, 1, -1, 1);

        int camLocation = glGetUniformLocation(shader.id(), "camera");
        glUniformMatrix3fv(camLocation, 1, GL_FALSE, camera.values);

        int viewLocation = glGetUniformLocation(shader.id(), "view");
        glUniformMatrix3fv(viewLocation, 1, GL_FALSE, view->values);


        // render the cube
        glBindVertexArray(VAO1);

        iter = root;
        if ( iter->next != 0 ) {

          float layer = 1.0;
          float tot_mem = iter->next->mem;
          iter = root;
          //set buffer data to be smallest possible to still contain the biggest shape
          glBufferData(GL_ARRAY_BUFFER, max_mem, NULL, GL_STATIC_DRAW);

          while (iter -> next != 0){
            if (iter->next->mem > max_mem){
              max_mem = iter->next->mem;
              break;
            }
            //use each nodes own matrices
            int modelLocation = glGetUniformLocation(shader.id(), "model");
            glUniformMatrix3fv(modelLocation, 1, GL_FALSE, iter->next->transform.values);
            int transLocation = glGetUniformLocation(shader.id(), "translate");
            glUniformMatrix3fv(transLocation, 1, GL_FALSE, iter->next->translate.values);
            if(iter->next->selected){
              //selection highlighting
              std::vector<float> temp = iter->next->points;
              for (int i = 0; i < temp.size(); i+=5){
                temp[i] = temp[i] * 1.05;
                temp[i+1] = temp[i+1] * 1.05;
                temp[i+2] = 0;
                temp[i+3] = 1;
                temp[i+4] = 0;
              }
              glBufferSubData(GL_ARRAY_BUFFER, 0, temp.size()*sizeof(float), &temp[0]);
              glDrawArrays(GL_TRIANGLES, 0, temp.size()/5);

            }
            glBufferSubData(GL_ARRAY_BUFFER, 0, iter->next->mem, &iter->next->points[0]);

            glDrawArrays(GL_TRIANGLES, 0, (iter->next->mem/sizeof(float))/5);

            iter = iter->next;
          }
          glBufferData(GL_ARRAY_BUFFER, tot_mem, NULL, GL_STATIC_DRAW);

        }
        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();

        glfwSetMouseButtonCallback(window, mouse_button_callback);

        //mode control
        if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS && spacePressed == false){
          mode+=1;
          spacePressed = true;
        }
        if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE && spacePressed == true){
          //release spacebar
          spacePressed = false;
        }
        if (mode > 2) {
          mode = 0;
        }

        glfwSetKeyCallback(window, color_callback);
        if (mode == 0){
          mode0(window);
        }
        if (mode == 1){
          mode1(window);
        }
        if (mode == 2){
          mode2(window);
        }

        if (isPressed(window, GLFW_KEY_ENTER)){
          //make a shape permanent
          if (mode == 1 || mode == 0){
            if (last->next != 0){
              last = last->next;
              last->selected = false;
            }
          }
        }

        click = false;
}

    glfwTerminate();
    return 0;
}
