#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec3 aColor;

uniform mat3 model;
uniform mat3 camera = mat3(1,0,0,0,1,0,0,0,1);
uniform mat3 view;
uniform mat3 translate = mat3(1,0,0,0,1,0,0,0,1);

out vec3 myColor;

void main() {

    vec3 bPos =  vec3(aPos, 1.0);
    // bPos = vec3(view * vec4(bPos, 1.0));
    bPos = view * translate * model * camera * bPos;
    gl_Position = vec4(bPos, 1.0);
    myColor = aColor;
}
