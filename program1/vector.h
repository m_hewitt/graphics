#ifndef _CSCI441_VECTOR_H_
#define _CSCI441_VECTOR_H_

#include <sstream>

class Vector {
    const static int X=0;
    const static int Y=1;
    const static int W=2;
    const static int N_COORDS = 3;

    float det(float a, float b, float c, float d) const {
        return  a*d - b*c;
    }

public:
    float values[N_COORDS];

    Vector(float x, float y, float w=1) : values{x, y, w} { }

    Vector operator-(const Vector& v) const {
        return Vector(
                values[X] - v.values[X],
                values[Y] - v.values[Y],
                values[W] - v.values[W]);
    }
    float operator*(const Vector& v) const {
      return ((values[X] * v.values[X]) + (values[Y] * v.values[Y]));
    }

    float x() const { return values[X]; }
    float y() const { return values[Y]; }
    float w() const { return values[W]; }
    float coord(int idx) { return values[idx % N_COORDS]; }

    Vector scale(double s) const {
        return Vector(
                s*values[X],
                s*values[Y]
                );
    }

    Vector normalized() const {
        double inv_norm = 1/norm();
        return scale(inv_norm);
    }
    
    void setxy (float x, float y){
      values[X] = x;
      values[Y] = y;
    }

    double norm() const {
        double n_sq = 0;
        for (int i = 0; i < N_COORDS; ++i) {
            n_sq += values[i]*values[i];
        }
        return sqrt(n_sq);
    }

    std::string to_string()  const {
        std::ostringstream os;
        for (int i = 0; i < N_COORDS; ++i) {
            os << values[i] << " ";
        }
        return os.str();
    }

    friend std::ostream& operator<<(std::ostream& os, const Vector& v) {
        os << v.to_string();
        return os;
    }
};

#endif
