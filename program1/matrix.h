#ifndef _CSCI441_MATRIX_H_
#define _CSCI441_MATRIX_H_

#include <sstream>

#include "vector.h"

class Matrix {
private:
    unsigned int idx(unsigned int r, unsigned int c) const {
        return r + c*3;
    }

public:
    float values[9];

    Matrix() {
        set_to_identity();
    };

    void scale(float x, float y) {
        set_to_identity();
        values[idx(0,0)] = x;
        values[idx(1,1)] = y;
    }

    void rotate_z(float theta) {
        double theta_radians = theta*M_PI/180.0;

        set_to_identity();
        values[idx(0,0)] = std::cos(theta_radians);
        values[idx(0,1)] = -std::sin(theta_radians);
        values[idx(1,0)] = std::sin(theta_radians);
        values[idx(1,1)] = std::cos(theta_radians);

    }

    void translate(float x, float y) {
        set_to_identity();
        values[idx(0,2)] = x;
        values[idx(1,2)] = y;
    }

    void ortho(float l, float r, float b, float t, float n, float f) {
        set_to_identity();
        values[idx(0,0)] = 2 / (r-l);
        values[idx(0,3)] = -(r+l) / (r-l);

        values[idx(1,1)] = 2 / (t-b);
        values[idx(1,3)] = -(t+b) / (t-b);

        values[idx(2,2)] = -2 / (f-n);
        values[idx(2,3)] = -(f+n) / (f-n);
    }


    Matrix operator*(const Matrix& m) const {
        Matrix ret;
        mult(ret.values, values, m.values);
        return ret;
    }

    //this one is mine
    Vector operator*(const Vector& v) const {
        Vector ret(1,1,1);
        float x = v.x() * values[idx(0,0)] + v.y() * values[idx(0,1)] + values[idx(0,2)];
        float y = v.x() * values[idx(1,0)] + v.y() * values[idx(1,1)] + values[idx(1,2)];
        ret.setxy(x, y);
        return ret;
    }

    std::string to_string()  const {
        std::ostringstream os;
        for (int row = 0; row < 3; ++row) {
            for (int col = 0; col < 3; ++col) {
                os << values[idx(row, col)] << " ";
            }
            os << std::endl;
        }
        return os.str();
    }

    friend std::ostream& operator<<(std::ostream& os, const Matrix& m) {
        os << m.to_string();
        return os;
    }
    Matrix inverse(){
      Matrix result;
      double determinant =    +values[idx(0,0)]*(values[idx(1,1)]*values[idx(2,2)]-values[idx(2,1)]*values[idx(1,2)])
                              -values[idx(0,1)]*(values[idx(1,0)]*values[idx(2,2)]-values[idx(1,2)]*values[idx(2,0)])
                              +values[idx(0,2)]*(values[idx(1,0)]*values[idx(2,1)]-values[idx(1,1)]*values[idx(2,0)]);
      double invdet = 1/determinant;
      result.values[idx(0,0)] =  (values[idx(1,1)]*values[idx(2,2)]-values[idx(2,1)]*values[idx(1,2)])*invdet;
      result.values[idx(0,1)] = -(values[idx(0,1)]*values[idx(2,2)]+values[idx(0,2)]*values[idx(2,1)])*invdet;
      result.values[idx(0,2)] = -(values[idx(0,1)]*values[idx(1,2)]+values[idx(0,2)]*values[idx(1,1)])*invdet;
      result.values[idx(1,0)] = -(values[idx(1,0)]*values[idx(2,2)]+values[idx(1,2)]*values[idx(2,0)])*invdet;
      result.values[idx(1,1)] =  (values[idx(0,0)]*values[idx(2,2)]-values[idx(0,2)]*values[idx(2,0)])*invdet;
      result.values[idx(1,2)] = -(values[idx(0,0)]*values[idx(1,2)]+values[idx(1,0)]*values[idx(0,2)])*invdet;
      result.values[idx(2,0)] = -(values[idx(1,0)]*values[idx(2,1)]+values[idx(2,0)]*values[idx(1,1)])*invdet;
      result.values[idx(2,1)] = -(values[idx(0,0)]*values[idx(2,1)]+values[idx(2,0)]*values[idx(0,1)])*invdet;
      result.values[idx(2,2)] =  (values[idx(0,0)]*values[idx(1,1)]-values[idx(1,0)]*values[idx(0,1)])*invdet;
      return result;
    }

private:

    double dot(const float* m1, unsigned int row, const float* m2, int col) const {
        double dot = 0;
        for (int i = 0; i < 3; ++i) {
            dot += m1[idx(row,i)]*m2[idx(i,col)];
        }
        return dot;
    }

    void mult(float* target, const float* const m1, const float* const m2) const {
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                target[idx(i,j)] = dot(m1, i, m2, j);
            }
        }
    }

    void set_to_identity() {
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                values[idx(i,j)] = i==j;
            }
        }
    }

};

#endif
