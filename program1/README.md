MODE 0 - FREE HAND

Left click adds a new point to the shape
When finished, Right click draws it at the origin
It will be permanent once you hit enter

______________________________
MODE 1 - PRIMITIVES

7 = Circle
8 = Square
9 = Triangle
The shape will be drawn at the origin
It will become permanent once you hit enter

______________________________
MODE 2 - GROUPING

Add and remove selection by left clicking
currently selected shapes signified by a green highlight
to group currently selected shapes, right click
to ungroup a shape, press enter while it is selected
Full Disclosure: Grouping gets weird with camera movement

______________________________
TRANSFORMATIONS

Transformations will be done to all selected shapes
New shapes that have not become permanent start selected
Rotation - [, ]
Scale x - z, x
Scale y - c, v
Translate - WASD, or MouseDrag(shape does not have to be selected)
Camera Movement - Arrow Keys

______________________________
COLOR

Changes applied to all selected shapes
RGB increased with 1,2,3
RGB decreased with SHIFT+(1,2,3)

______________________________
MY SHAPE

I wanted to make a shape that was made up of mostly primitives, because its not really easy to draw without seeing what you are doing. I also wanted to be able to make something recognizable. It came down to a Jack-o-Lantern or a Snowman. I decided on the former, because it also has a couple organic shapes. I wanted to show off that aspect as well, drawing both the stem and the mouth by hand. Since it is a object as a whole as well, I could scale and rotate it while maintaining its shape, once all of the parts were grouped.
