//barycentric calculations : https://gamedev.stackexchange.com/questions/23743/whats-the-most-efficient-way-to-find-barycentric-coordinates
//Authors: Michael Hewitt, Grace Walkuski
//CSCI 441
//Lab 1
//1-25-2018

#include "bitmap_image.hpp"
#include "string"

class Point{
//Class for holding points, coordinates and colors
public:
  float x;
  float y;
  float r;
  float g;
  float b;

  Point (float xx, float yy, float rr, float gg, float bb) : x(xx), y(yy), r(rr), g(gg), b(bb){
    r=r*255;
    g=g*255;
    b=b*255;
  }
  Point(){

  }
  float dot(Point p){
    float d_product = x*p.x + y*p.y;
    return d_product;
  }
  Point minus(Point p){
    Point difference = Point(x-p.x, y-p.y, 1, 1, 1);
    return difference;
  }
};

int main()
{
  std::cout << "Enter 3 points (enter a point as x,y:r,g,b):" << std::endl;
  Point bounds[3];
  for (int i = 0; i < 3; i++){
    //input vertices
    std::string temp;
    char sep;
    std::cin >> bounds[i].x >> sep >> bounds[i].y >> sep >>bounds[i].r >> sep >> bounds[i].g >> sep >> bounds[i].b;
  }
  //find bounds for square
  float lbound=bounds[0].x;
  float rbound=bounds[0].x;
  float ubound=bounds[0].y;
  float lowbound=bounds[0].y;
  for (int i=1;i<3;i++){
    if (bounds[i].x < lbound){
      lbound=bounds[i].x;
    }
    if (bounds[i].x > rbound){
      rbound=bounds[i].x;
    }
    if (bounds[i].y < ubound){
      ubound=bounds[i].y;
    }
    if (bounds[i].y > lowbound){
      lowbound=bounds[i].y;
    }
  }
  bitmap_image image(640,640);
  //find the lines between each vertex
  Point gamma = bounds[1].minus(bounds[0]);
  Point beta = bounds[2].minus(bounds[0]);
  float gg = gamma.dot(gamma);
  float gb = gamma.dot(beta);
  float bb = beta.dot(beta);
  for (int x=0;x<640;x++){
    for (int y=0;y<640;y++){
      if (x>lbound && x < rbound && y > ubound && y < lowbound){
        //convert point to barycentric
        Point cur = Point(x,y,1,1,1);
        Point alpha = cur.minus(bounds[0]);
        float ga = gamma.dot(alpha);
        float ba = beta.dot(alpha);
        float denom = (gg * bb) - (gb * gb);
        float l1 = ((bb * ga) - (gb * ba)) / denom;
        float l2 = ((gg * ba) - (gb * ga)) / denom;
        float l3 = 1.0f - l1 - l2;
        if (l1 >= 0 && l2 >= 0 && l3 >= 0 && l1 <= 1 && l2 <= 1 && l3 <= 1){
          //using barycentric coordinates as weights
          float red = (bounds[0].r * l3 + bounds[1].r * l1 + bounds[2].r * l2);
          float green = (bounds[0].g * l3 + bounds[1].g * l1 + bounds[2].g * l2);
          float blue = (bounds[0].b * l3 + bounds[1].b * l1 + bounds[2].b * l2);
          image.set_pixel(x,y,red*255,green*255,blue*255);
        }
      }
    }
  }
  image.save_image("bitmap.bmp");
}
