#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <unordered_map>
#include <set>
#include <numeric>
#include <algorithm>


#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"
#include "model.h"
#include "camera.h"
#include "renderer.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix processModel(const Matrix& model, GLFWwindow *window) {
    Matrix trans;

    const float ROT = 1;
    const float SCALE = .05;
    const float TRANS = .01;

    // ROTATE
    if (isPressed(window, GLFW_KEY_U)) { trans.rotate_x(-ROT); }
    else if (isPressed(window, GLFW_KEY_I)) { trans.rotate_x(ROT); }
    else if (isPressed(window, GLFW_KEY_O)) { trans.rotate_y(-ROT); }
    else if (isPressed(window, GLFW_KEY_P)) { trans.rotate_y(ROT); }
    else if (isPressed(window, '[')) { trans.rotate_z(-ROT); }
    else if (isPressed(window, ']')) { trans.rotate_z(ROT); }
    // SCALE
    else if (isPressed(window, '-')) { trans.scale(1-SCALE, 1-SCALE, 1-SCALE); }
    else if (isPressed(window, '=')) { trans.scale(1+SCALE, 1+SCALE, 1+SCALE); }
    // TRANSLATE
    else if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0, TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0, -TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_LEFT)) { trans.translate(-TRANS, 0, 0); }
    else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.translate(TRANS, 0, 0); }
    else if (isPressed(window, ',')) { trans.translate(0,0,TRANS); }
    else if (isPressed(window, '.')) { trans.translate(0,0,-TRANS); }

    return trans * model;
}

void processInput(Matrix& model, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
    model = processModel(model, window);
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

template <typename Coords, typename Edges>
std::vector<float> smoothNormal(Coords& coords, Edges& edges){
    std::unordered_map<int, Vector> normals;
    std::vector<float>::iterator it;
    for(auto kv : edges) {
      Vector total(0,0,0);
      for (int i = 0; i < kv.second.size(); i+=3){
        //normals for all triangles that a certain point is in
        Vector v1(coords[3*kv.second[i]], coords[3*kv.second[i]+1], coords[3*kv.second[i]+2]);
        Vector v2(coords[3*kv.second[i+1]], coords[3*kv.second[i+1]+1], coords[3*kv.second[i+1]+2]);
        Vector v3(coords[3*kv.second[i+2]], coords[3*kv.second[i+2]+1], coords[3*kv.second[i+2]+2]);
        v1 = v3 - v1;
        v2 = v3 - v2;
        v1 = v1.cross(v2);
        v1 = v1.normalized();
        //add normal to total normal
        total = total + v1;
      }
      it = coords.begin();
      //add normals to list of normals
      normals.insert({{kv.first, total}});
  }
  std::vector<float> smoothCoords;
  for(int i = 0; i < coords.size(); i+=3){
    int normalPos = i/3;
    //rebuild coordinate list with color and normals
    std::unordered_map<int, Vector>::iterator iter = normals.find(normalPos);
    smoothCoords.push_back(coords[i]);
    smoothCoords.push_back(coords[i+1]);
    smoothCoords.push_back(coords[i+2]);
    smoothCoords.push_back(1);
    smoothCoords.push_back(1);
    smoothCoords.push_back(1);
    smoothCoords.push_back(iter->second.x());
    smoothCoords.push_back(iter->second.y());
    smoothCoords.push_back(iter->second.z());
  }
  return smoothCoords;
}

template <typename Coords, typename Indices>
std::vector<float> flatNormal(Coords& coords, Indices& indices){
  std::vector<float> flatCoords;
  for (int i = 0; i < indices.size(); i+=3){
    //find normals for every triangle
    Vector v1(coords[3*indices[i]], coords[3*indices[i]+1], coords[3*indices[i]+2]);
    Vector v2(coords[3*indices[i+1]], coords[3*indices[i+1]+1], coords[3*indices[i+1]+2]);
    Vector v3(coords[3*indices[i+2]], coords[3*indices[i+2]+1], coords[3*indices[i+2]+2]);
    v1 = v3 - v1;
    v2 = v3 - v2;
    v1 = v1.cross(v2);
    v1 = v1.normalized();
    for (int j = 0; j < 3; j++){
      //create coordinates for every triangle
      flatCoords.push_back(coords[3 * indices[i+j]]);
      flatCoords.push_back(coords[3 * indices[i+j]+1]);
      flatCoords.push_back(coords[3 * indices[i+j]+2]);
      flatCoords.push_back(1);
      flatCoords.push_back(1);
      flatCoords.push_back(1);
      flatCoords.push_back(v1.x());
      flatCoords.push_back(v1.y());
      flatCoords.push_back(v1.z());
    }
  }
  return flatCoords;
}
int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-lab", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }
    std::ifstream infile("models/teddy.obj");
    std::vector<float> coords;
    std::vector<unsigned int> indices;
    std::vector<float> smoothCoords;
    std::vector<float> flatCoords;
    std::unordered_map<int, std::vector<int>> edges;
    float x, y, z;
    char type;
    std::string line;
    while (std::getline(infile, line))
    {
      std::istringstream iss(line);
      if ((iss >> type >> x >> y >> z)) {
        if (type == 'v'){
          //build coordinate list
          coords.push_back(x/20);
          coords.push_back(y/20);
          coords.push_back(z/20);
        }
        if (type == 'f'){
          //build index list
          indices.push_back(x-1);
          indices.push_back(y-1);
          indices.push_back(z-1);
          //map vertices to all triangles it is a part of
          edges[x-1].push_back((int)x-1);
          edges[x-1].push_back((int)y-1);
          edges[x-1].push_back((int)z-1);
          edges[y-1].push_back((int)x-1);
          edges[y-1].push_back((int)y-1);
          edges[y-1].push_back((int)z-1);
          edges[z-1].push_back((int)x-1);
          edges[z-1].push_back((int)y-1);
          edges[z-1].push_back((int)z-1);
        }
      }
    }
    smoothCoords = smoothNormal(coords, edges);
    flatCoords = flatNormal(coords, indices);
    //flat indices is list of all numbers 0 to coordinate size
    //flat shading does not need elements, so every coordinate is used once
    std::vector<unsigned int> flatIndices(flatCoords.size()/9);
    std::iota(flatIndices.begin(),flatIndices.end(), 0);
    Matrix transform;
    //smooth object
    Model smooth(
            smoothCoords,
            indices,
            Shader("vert.glsl", "frag.glsl"),
            transform);
    //flat object
    Model flat(
            flatCoords,
            flatIndices,
            Shader("vert.glsl", "frag.glsl"),
            transform);

    Matrix t;
    Matrix projection;
    projection.perspective(45, 1, .01, 10);

    Camera camera;
    camera.projection = projection;
    camera.eye = Vector(0, 0, 3);
    camera.origin = Vector(0, 0, 0);
    camera.up = Vector(0, 1, 0);

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    // create a renderer
    Renderer renderer;
    Model* current = &smooth;
    // set the light position
    Vector lightPos(0.0f, 0.0f, 0.0f);
    bool spacePressed = false;
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(transform, window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // render the object and the floor
        renderer.render(camera, *current, lightPos);
        //renderer.render(camera, floor, lightPos);

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();

        if (isPressed(window, GLFW_KEY_W)) { lightPos = lightPos + *new Vector(0, .05, 0); }
        if (isPressed(window, GLFW_KEY_A)) { lightPos = lightPos + *new Vector(-.05, 0, 0); }
        if (isPressed(window, GLFW_KEY_S)) { lightPos = lightPos + *new Vector(0, -.05, 0); }
        if (isPressed(window, GLFW_KEY_D)) { lightPos = lightPos + *new Vector(.05, 0, 0); }
        if (isPressed(window, GLFW_KEY_Z)) { lightPos = lightPos + *new Vector(0, 0, -.05); }
        if (isPressed(window, GLFW_KEY_X)) { lightPos = lightPos + *new Vector(0, 0, .05); }
        if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS && spacePressed == false){
        if (current == &flat){
          current = &smooth;
        }
        else{
          current = &flat;
        }
        spacePressed = true;
      }
      if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE && spacePressed == true){
        //release spacebar
        spacePressed = false;
      }
    }

    glfwTerminate();
    return 0;
}
