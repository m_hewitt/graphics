**CONTROLS**
W,S - move character forward and backward
+,- - scale character
U,I,O,P,[,] - rotate character
mouse - look
spacebar - change view

**INFO**
Models found in models folder, filename hardcoded in
Mazes in txt format in mazes folder
