#ifndef _CSCI441_MODEL_H_
#define _CSCI441_MODEL_H_
#include <csci441/vector.h>

class Model {

public:
    GLuint vbo;
    GLuint vao;
    GLuint ebo;
    Shader shader;
    Matrix* translate;
    Matrix* rotate;
    int size;
    int polysize;

    template <typename Coords, typename Indices>
    Model(const Coords& coords, const Indices& indices, const Shader& shader_in, Matrix& t, Matrix& r) : shader(shader_in) {
        size = coords.size()*sizeof(float);
        polysize = indices.size();
        translate = &t;
        rotate = &r;
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        // copy vertex data
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, size, coords.data(), GL_STATIC_DRAW);

        glGenBuffers(1, &ebo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, polysize * sizeof(unsigned int), indices.data(), GL_STATIC_DRAW);
        // describe vertex layout


        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                (void*)(0*sizeof(float)));
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                (void*)(3*sizeof(float)));
        glEnableVertexAttribArray(1);

        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                (void*)(6*sizeof(float)));
        glEnableVertexAttribArray(2);
    }
    // findNormals(){
    //
    // }
};

#endif
