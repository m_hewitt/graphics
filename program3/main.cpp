#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtx/normal.hpp>
#include <sstream>
#include <glm/gtc/noise.hpp>
#include <glm/ext.hpp>
#include "bitmap_image.hpp"

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;
    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

struct Triangle{
  glm::vec3 p1;
  glm::vec3 p2;
  glm::vec3 p3;
  glm::vec3 color;
  Triangle(const glm::vec3& p1, const glm::vec3& p2, const glm::vec3& p3, const glm::vec3& color)
        : p1(p1), p2(p2), p3(p3), color(color){}
};
struct Sphere {
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0),
            float radius=0,
            const glm::vec3& color=glm::vec3(0,0,0))
        : center(center), radius(radius), color(color) {
            static int id_seed = 0;
            id = ++id_seed;
        }
};

struct Ray{
  glm::vec3 origin = glm::vec3(0, 0, 0);
  glm::vec3 direction = glm::vec3(0, 1, 0);
  glm::vec3 end;
  float a;
  float b;
  float c;
  Ray(glm::vec3 origin = glm::vec3(0, 0, 0),
  glm::vec3 direction = glm::vec3(0, 0, 1))
  : origin (origin), direction(direction){
    direction = glm::normalize(direction);

    end = glm::vec3(origin + (100.0f * direction));
  }
};
float intersect(Ray r, Triangle t, glm::vec3 light){

  glm::vec3 N = glm::triangleNormal(t.p1, t.p2, t.p3);

  float D = glm::dot(N, t.p1);
  float s = (glm::dot(N, r.origin) + D) / glm::dot(N, r.direction);
  glm::vec3 P = glm::vec3(r.origin + (s * r.direction));
  glm::vec3 edge0 = t.p2 - t.p1;
  glm::vec3 edge1 = t.p3 - t.p2;
  glm::vec3 edge2 = t.p1 - t.p3;
  glm::vec3 C0 = P - t.p1;
  glm::vec3 C1 = P - t.p2;
  glm::vec3 C2 = P - t.p3;

  if (s < 0) return false;
  if (glm::dot(N, r.direction) == 0){
    return false;
  }

  if (glm::dot(N, glm::cross(edge0, C0)) > 0 &&
      glm::dot(N, glm::cross(edge1, C1)) > 0 &&
      glm::dot(N, glm::cross(edge2, C2)) > 0){
        float noise = (glm::perlin(P) + 1) / .5;
        //noise = noise % 3;
        glm::vec3 lightDir = glm::normalize(light - P);
        float diff = fmax(glm::dot(N, lightDir), 0.00001);
        noise = int(noise * 255) % 256;
        diff = fmax((diff * noise), 0.00001);
        return diff;
      }

  return false;

}
void ortho(Ray* ray, float ui, float vj){
  *ray = Ray(glm::vec3(-2, ui, vj), glm::vec3(2, 0, 0));

}
void perspective(Ray* ray, float ui, float vj){
  *ray = Ray(glm::vec3(-3, 0, 0), glm::vec3(3,ui,vj));

}
void render(bitmap_image& image, const std::vector<Triangle>& world) {
  float ui;
  float vj;
  float diff;
  glm::vec3 colormap[640][480];
  Ray ray;
      for (int i = 0; i < 640; i++){
        for (int j = 0; j < 480; j++){
          image.set_pixel(i, j, 75, 156, 211);
          colormap[i][j] = glm::vec3(75,156,211);
          ui = -5 + (10)*(i+.5) / 640;
          vj = 5 + (-10)*(j+.5) / 480;
          //perspective(&ray, ui, vj);
          ortho(&ray, ui, vj);
          float closest = 1000;
          for(int k = 0; k < world.size(); k++){
            diff = intersect(ray, world[k], glm::vec3(-1, -3, 0));
            if(diff){

              image.set_pixel(i, j, diff * world[k].color.x, diff * world[k].color.y, diff * world[k].color.z);
              colormap[i][j] = glm::vec3(diff * world[k].color.x, diff * world[k].color.y, diff * world[k].color.z);

            }
          }
        }
      }
      float r;
      float g;
      float b;
      for (int i = 1; i < 639; i++){
        for (int j = 1; j < 479; j++){
          r = colormap[i][j].x + colormap[i][j - 1].x + colormap[i - 1][j].x + colormap[i][j + 1].x + colormap[i + 1][j].x;
          g = colormap[i][j].y + colormap[i][j - 1].y + colormap[i - 1][j].y + colormap[i][j + 1].y + colormap[i + 1][j].y;
          b = colormap[i][j].z + colormap[i][j - 1].z + colormap[i - 1][j].z + colormap[i][j + 1].z + colormap[i + 1][j].z;
          r = r / 5;
          g = g / 5;
          b = b / 5;
          image.set_pixel(i, j, r, g, b);
        }
      }
}

int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);

    // build world
    std::vector<Triangle> world;
    std::ifstream infile("models/human.obj");
    std::vector<glm::vec3> coords;
    float x, y, z;
    char type;
    std::string line;
    //world.push_back(Triangle(glm::vec3(5,3,3),glm::vec3(5,3,-3),glm::vec3(5,-3,0), glm::vec3(1,1,1)));
    //world.push_back(Triangle(glm::vec3(-3,2,2),glm::vec3(-1,1,1),glm::vec3(-1,0,.5), glm::vec3(0,1,1)));

    while (std::getline(infile, line))
    {
      std::istringstream iss(line);
      if ((iss >> type >> x >> y >> z)) {
        if (type == 'v'){
          coords.push_back(glm::vec3(x/2 + 5, y/2, z/2 - 6 ));
        }
        if (type == 'f'){
          world.push_back(Triangle(coords[x-1],coords[y-1],coords[z-1], glm::vec3(1,1,1)));

        }
      }
    }

    // render the world
    render(image, world);

    image.save_image("ray-traced.bmp");
    std::cout << "Success" << std::endl;
}
