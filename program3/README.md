Uses an obj file to load triangles

Detects if rays intersect any triangles in the world

performs perlin noise to apply a texture for each point

multiplies texture with diffuse light calculation

sets image pixel to result of ray intersection

screenshots of rendered images can be found in the 'img' folder
