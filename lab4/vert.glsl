#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
uniform vec3 view;
uniform mat4 transform;
uniform mat4 projection;
out vec3 ourColor;

void main() {
    vec4 trans = transform * vec4(aPos, 1.0);
    vec4 bPos = trans + vec4(view, 0);
    bPos = projection * bPos;
    gl_Position = bPos;
    ourColor = aColor;
}
