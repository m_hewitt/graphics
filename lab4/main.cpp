
//Michael Hewitt, Grace Walkuski
//CSCI441
//02-22-2018


#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window, Shader &shader) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    //cylinder
    int stepCount = 30;
    float radius = .5;
    float height = 1;

    int circleSize = stepCount * 18;
    float cylinder[circleSize * 4];
    float circle1[circleSize];
    float circle2[circleSize];
    float rectangles[circleSize * 2];
    float st = 2 * 3.14159265/stepCount;
    int index = 0;
    float step;
    for (int i = 0; i < stepCount; i++){
      //Bottom Circle
        index = i * 18;
        step = st * i;
        circle1[index] = radius * cos(step);
        circle1[index + 1] = radius * sin(step);
        circle1[index + 2] = -height/2;
        circle1[index + 3] = 0;
        circle1[index + 4] = 0;
        circle1[index + 5] = 1;

        circle1[index + 6] = 0;
        circle1[index + 7] = 0;
        circle1[index + 8] = -height/2;
        circle1[index + 9] = 0;
        circle1[index + 10] = 0;
        circle1[index + 11] = 1;

        step = st * (i + 1);
        circle1[index +12] = radius * cos(step);
        circle1[index + 13] = radius * sin(step);
        circle1[index + 14] = -height/2;
        circle1[index + 15] = 0;
        circle1[index + 16] = 0;
        circle1[index + 17] = 1;
    }
    for (int i = 0; i < stepCount; i++){
      //Top Circle
        index = i * 18;
        step = st * i;
        circle2[index] = radius * cos(step);
        circle2[index + 1] = radius * sin(step);
        circle2[index + 2] = height/2;
        circle2[index + 3] = 0;
        circle2[index + 4] = 0;
        circle2[index + 5] = 1;

        circle2[index + 6] = 0;
        circle2[index + 7] = 0;
        circle2[index + 8] = height/2;
        circle2[index + 9] = 0;
        circle2[index + 10] = 0;
        circle2[index + 11] = 1;

        step = st * (i + 1);
        circle2[index +12] = radius * cos(step);
        circle2[index + 13] = radius * sin(step);
        circle2[index + 14] = height/2;
        circle2[index + 15] = 0;
        circle2[index + 16] = 0;
        circle2[index + 17] = 1;

        //Connecting Circles with rectangles
        for (int j = 0; j < 6; j ++){
          rectangles[(i*36)+j] = circle1[index+j];
        }
        for (int j = 0; j < 6; j ++){
          rectangles[(i*36) + 6 + j] = circle2[index + j];
        }
        for (int j = 0; j < 6; j ++){
          rectangles[(i*36) + 12 + j] = circle2[index + 12 + j];
        }
        for (int j = 0; j < 6; j ++){
          rectangles[(36*i) + 18 + j] = circle2[index + 12 + j];
        }
        for (int j = 0; j < 6; j ++){
          rectangles[(i*36) + 24 + j] = circle1[index + 12 + j];
        }
        for (int j = 0; j < 6; j ++){
          rectangles[(i*36) + 30 + j] = circle1[index + j];
        }

    }

    //color the side red
    for (int blue = 5; blue < circleSize * 2; blue+=6){
      rectangles[blue] = 0;
      rectangles[blue - 2] = 1;
    }

    //add cylinder to buffer matrix
    for (int i = 0; i < circleSize; i++){
      cylinder [i] = circle1 [i];
    }
    for (int i = 0; i < circleSize; i++){
      cylinder [i + (circleSize)] = circle2 [i];
    }
    for ( int i = 0; i < circleSize * 2; i++){
      cylinder [i + (circleSize * 2)] = rectangles [i];
    }


    //Sphere
    stepCount = 50;
    radius = .5;

    circleSize = stepCount * 12;
    float sphere[stepCount * stepCount * 36];
    float cir1[circleSize];
    float cir2[stepCount * 24];
    int point;
    int start;
    st = 2 * 3.14159265/stepCount;
    float st2 = 3.14159265/stepCount;
    index = 0;
    for (int i = 0; i < stepCount; i++){
      //Circle around the z-axis (latitude lines)
        index = i * 12;

        step = st * i;
        cir1[index] = radius * cos(step);
        cir1[index + 1] = radius * sin(step);
        cir1[index + 2] = 0;
        cir1[index + 3] = 0;
        cir1[index + 4] = 0;
        cir1[index + 5] = 1;

        step = st * (i + 1);
        cir1[index + 6] = radius * cos(step);
        cir1[index + 7] = radius * sin(step);
        cir1[index + 8] = 0;
        cir1[index + 9] = 0;
        cir1[index + 10] = 0;
        cir1[index + 11] = 1;

        for (int j = 0; j < stepCount; j++){
          //rotate circle around the y-axis (longitude lines)
          point = j * 24;

          step = st2 * j;
          cir2[point] = cir1[index] * sin(step);
          cir2[point+1] = cir1[index+1] * sin(step);
          cir2[point+2] = radius * cos(step);
          cir2[point+3] = 0;
          cir2[point+4] = 0;
          cir2[point+5] = 1;

          step = st2 * (j+1);
          cir2[point+6] = cir1[index] * sin(step);
          cir2[point+7] = cir1[index+1] * sin(step);
          cir2[point+8] = radius * cos(step);
          cir2[point+9] = 0;
          cir2[point+10] = 1;
          cir2[point+11] = 0;

          step = st2 * j;
          cir2[point+12] = cir1[index+6] * sin(step);
          cir2[point+13] = cir1[index+7] * sin(step);
          cir2[point+14] = radius * cos(step);
          cir2[point+15] = 1;
          cir2[point+16] = 0;
          cir2[point+17] = 0;

          step = st2 * (j+1);
          cir2[point+18] = cir1[index+6] * sin(step);
          cir2[point+19] = cir1[index+7] * sin(step);
          cir2[point+20] = radius * cos(step);
          cir2[point+21] = 0;
          cir2[point+22] = 0;
          cir2[point+23] = 1;

          //add sphere points to buffer matrix
          start = (((i * stepCount) + j) * 36);
          for (int k = 0; k < 18; k++){
            sphere[start+k] = cir2[point+k];
          }
          for (int k = 18; k < 36; k++){
            sphere[start+k] = cir2[point+k-12];
          }
        }
    }


    float vertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f
    };
    // copy vertex data
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // describe vertex layout
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    //cylinder data
    GLuint CylVBO;
    glGenBuffers(1, &CylVBO);
    glBindBuffer(GL_ARRAY_BUFFER, CylVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylinder), cylinder, GL_STATIC_DRAW);

    GLuint CylVAO;
    glGenVertexArrays(1, &CylVAO);
    glBindVertexArray(CylVAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    //sphere data
    GLuint sphVBO;
    glGenBuffers(1, &sphVBO);
    glBindBuffer(GL_ARRAY_BUFFER, sphVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere), sphere, GL_STATIC_DRAW);

    GLuint sphVAO;
    glGenVertexArrays(1, &sphVAO);
    glBindVertexArray(sphVAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("vert.glsl", "frag.glsl");

    // setup the textures
    shader.use();

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);
    float view[] = {0,1,0};

    //translations
    float yMove = 0;
    float xMove = 0;
    float zMove = -4;

    //scale
    float scale = 1;

    //rotations
    float rotatex = 0;
    float rotatey = 0;
    float rotatez = 0;

    //aspect ratio
    float ar = 640/480;

    //projection mode
    int mode = 0;

    //backslash pressed
    bool pressed = false;

    //projection matrix
    float project[] = {
      2/(2*ar),0,0,0,
      0,2,0,0,
      0,0,-101/99,-200/99,
      0,0,-1,0};

    //transformation matrix
    float transform[] = {
      1, 0, 0, 0,
      0, 1, 0, 0,
      0, 0, 1, 0,
      0, 0, 0, 1
    };

    //which primitive to draw : Cylinder = 0; Cube = 1; Sphere = 2
    int primitive = 0;

    //is spacebar pressed
    bool spacePressed = false;
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window, shader);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // activate shader
        shader.use();

        //set transormation matrix equal to variables
        view[1] = yMove;
        transform [0] = cos(rotatey) * scale * cos(rotatez);
        transform [1] = ((cos(rotatez) * -sin(rotatex) * -sin(rotatey)) + (sin(rotatez) * cos(rotatex))) * scale;
        transform [2] = ((cos(rotatez) * cos(rotatex) * -sin(rotatey)) + (sin(rotatez) * sin(rotatex))) * scale;
        transform [4] = -sin(rotatez) * cos(rotatey) * scale;
        transform [5] = ((-sin(rotatez) * -sin(rotatex) * -sin(rotatey)) + (cos(rotatez) * cos(rotatex))) * scale;
        transform [6] = ((-sin(rotatez) * cos(rotatex) * -sin(rotatey)) + (cos(rotatez) * sin(rotatex))) * scale;
        transform [8] = sin(rotatey) * scale;
        transform [9] = -sin(rotatex) * cos(rotatey) * scale;
        transform [10] = cos(rotatex) * scale * cos(rotatey);
        transform [12] = xMove;
        transform [14] = zMove;

        if (mode == 0){
          //perspective projection
          project[0] = 2/(2*ar);
          project[5] = 2;
          project[10] = -101/99;
          project[11] = -1;
          project[14] = -200/99;
          project[15] = 0;
        }
        else if (mode == 1){
          //orthogonal projection
          project[0] = .5;
          project[5] = .7;
          project[10] = -2/99.9;
          project[11] = 0;
          project[14] = -100.1/99.9;
          project[15] = 1;
        }

        if (primitive == 0){
          //cylinder
          glBindVertexArray(CylVAO);
          glDrawArrays(GL_TRIANGLES, 0, sizeof(cylinder));
        }
        else if (primitive == 1){
          //cube
          glBindVertexArray(VAO);
          glDrawArrays(GL_TRIANGLES, 0, sizeof(vertices));
        }
        else{
          //sphere
          glBindVertexArray(sphVAO);
          glDrawArrays(GL_TRIANGLES, 0, sizeof(sphere));
        }
        // render the cube

        //pass in matrices to vertex shader
        GLint vertexLocation = glGetUniformLocation(shader.id(), "view");
        glUniform3fv(vertexLocation, 1.0 , view);
        GLint transLocation = glGetUniformLocation(shader.id(), "transform");
        glUniformMatrix4fv(transLocation, 1.0, false, transform);
        GLint projLocation = glGetUniformLocation(shader.id(), "projection");
        glUniformMatrix4fv(projLocation, 1.0, false, project);

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();

        if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS && yMove < 20){
          //move y-axis positive
          yMove += 0.05;
        }
        if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS && yMove >-20){
          //move y axis negative
          yMove += -0.05;
        }
        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS && xMove <20){
          //move x-axis negative
          xMove += 0.05;
        }
        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS && xMove >-20){
          //move x-axis negative
          xMove += -0.05;
        }
        if (glfwGetKey(window, GLFW_KEY_COMMA) == GLFW_PRESS && zMove >-20){
          //move z-axis negative
          zMove += -0.05;
        }
        if (glfwGetKey(window, GLFW_KEY_PERIOD) == GLFW_PRESS && zMove <20){
          //move z axis positive
          zMove += 0.05;
        }
        if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS && scale >-20){
          //scale down
          scale += -0.05;
        }
        if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS && scale <20){
          //scale up
          scale += 0.05;
        }
        if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS){
          //rotate around x-axis negative
          rotatex += -0.05;
        }
        if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS){
          //rotate around x-axis positive
          rotatex += 0.05;
        }
        if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS){
          //rotate around y-axis negative
          rotatey += -0.05;
        }
        if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS){
          //rotate around y-axis positive
          rotatey += 0.05;
        }
        if (glfwGetKey(window, GLFW_KEY_LEFT_BRACKET) == GLFW_PRESS){
          //rotate around z-axis negative
          rotatez += -0.05;
        }
        if (glfwGetKey(window, GLFW_KEY_RIGHT_BRACKET) == GLFW_PRESS){
          //rotate around z-axis positive
          rotatez += 0.05;
        }
        if (glfwGetKey(window, GLFW_KEY_BACKSLASH) == GLFW_PRESS && pressed == false){
          //switch projection mode
          mode+=1;
          pressed = true;
        }
        if (glfwGetKey(window, GLFW_KEY_BACKSLASH) == GLFW_RELEASE && pressed == true){
          //release backslash
          pressed = false;
        }
        if(mode >= 2){
          //cycle projection mode
          mode = 0;
        }
        if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS && spacePressed == false){
          //switch primitive
          primitive+=1;
          spacePressed = true;
        }
        if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE && spacePressed == true){
          //release spacebar
          spacePressed = false;
        }
        if(primitive >= 3){
          //cycle primitives
          primitive = 0;
        }
      }

    glfwTerminate();
    return 0;
}
