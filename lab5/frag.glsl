#version 330 core
in vec3 ourColor;
in vec3 newNormal;
in vec3 fragPos;
in vec3 view;

uniform vec3 lightPos;
out vec4 fragColor;


void main() {
    vec3 lightDir = normalize(lightPos - fragPos);
    
    vec3 viewDir = normalize(view - fragPos);

    vec3 reflectDir = reflect(-lightDir, newNormal);
    vec3 lightColor = vec3(1, 1, 1);
    float diff = max(dot(newNormal, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;
    float specularStrength = 1;
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    vec3 specular = specularStrength * spec * lightColor;
    vec3 ambient = vec3(.1, .1, .1);
    vec3 frag = (diffuse + ambient + specular) * ourColor;
    fragColor = vec4(frag, 1.0f);
}
