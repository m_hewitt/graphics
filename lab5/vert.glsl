#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec3 normal;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 camera;

out vec3 newNormal;
out vec3 ourColor;
out vec3 fragPos;
out vec3 view;

void main() {

    gl_Position = projection * camera * model * vec4(aPos, 1.0);
    newNormal = vec3(transpose(inverse(model)) * vec4(normal, 1.0));
    newNormal = normalize(newNormal);
    fragPos = vec3(model * vec4(aPos, 1.0));
    view = vec3(0,0,-20);
    ourColor = aColor;
}
