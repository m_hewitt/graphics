#include <iostream>

#include <glm/glm.hpp>

#include "bitmap_image.hpp"

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;
    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

struct Sphere {
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0),
            float radius=0,
            const glm::vec3& color=glm::vec3(0,0,0))
        : center(center), radius(radius), color(color) {
            static int id_seed = 0;
            id = ++id_seed;
        }
};

struct Ray{
  glm::vec3 origin = glm::vec3(0, 0, 0);
  glm::vec3 direction = glm::vec3(0, 1, 0);
  glm::vec3 end;
  float a;
  float b;
  float c;
  Ray(glm::vec3 origin = glm::vec3(0, 0, 0),
  glm::vec3 direction = glm::vec3(0, 0, -1))
  : origin (origin), direction(direction){
    direction = glm::normalize(direction);

    end = glm::vec3(origin + (100.0f * direction));
  }
};
float intersect(Ray r, Sphere s){
  float dx = r.end.x - r.origin.x;
  float dy = r.end.y - r.origin.y;
  float dz = r.end.z - r.origin.z;

  float a = dx*dx + dy*dy + dz*dz;
  float b = 2*dx*(r.origin.x-s.center.x) +  2*dy*(r.origin.y-s.center.y) +  2*dz*(r.origin.z-s.center.z);
  float c = pow(r.origin.x - s.center.x, 2) + pow(r.origin.y - s.center.y, 2) + pow(r.origin.z - s.center.z, 2) - (s.radius * s.radius);
  float dis = b*b - 4*a*c;

  float t1 = (-b + sqrt(dis))/(2*a);
  float t2 = (-b - sqrt(dis))/(2*a);
  if (t1 < t2){
    return (t1);
  }
  else {
    return (t2);
  }

}
void ortho(Ray* ray, float ui, float vj){
  *ray = Ray(glm::vec3(ui, vj, -1), glm::vec3(0, 0, 1));

}
void perspective(Ray* ray, float ui, float vj){
  *ray = Ray(glm::vec3(0, 0, -3), glm::vec3(ui,vj,3));

}
void render(bitmap_image& image, const std::vector<Sphere>& world) {
  float ui;
  float vj;
  Ray ray;
      for (int i = 0; i < 640; i++){
        for (int j = 0; j < 480; j++){
          image.set_pixel(i, j, 75, 156, 211);
          ui = -5 + (10)*(i+.5) / 640;
          vj = 5 + (-10)*(j+.5) / 480;
          //perspective(&ray, ui, vj);
          ortho(&ray, ui, vj);
          float closest = 1000;
          for(int k = 0; k < world.size(); k++){
            float temp = intersect(ray, world[k]);
            if(temp < closest && closest > 0){
              image.set_pixel(i, j, world[k].color.x * 255, world[k].color.y * 255, world[k].color.z * 255);
              closest = temp;
            }
          }
        }
      }
}

int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);

    // build world
    std::vector<Sphere> world = {
        Sphere(glm::vec3(0, 0, 1), 1, glm::vec3(1,1,0)),
        Sphere(glm::vec3(1, 1, 4), 2, glm::vec3(0,1,1)),
        Sphere(glm::vec3(2, 2, 6), 3, glm::vec3(1,0,1)),
    };

    // render the world
    render(image, world);

    image.save_image("ray-traced.bmp");
    std::cout << "Success" << std::endl;
}
