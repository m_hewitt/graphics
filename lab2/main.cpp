#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

//Authors: Michael Hewitt, Grace Walkuski
//CSCI 441
//Lab 2
//2-6-2018

/**
* BELOW IS A BUNCH OF HELPER CODE
* You do not need to understand what is going on with it, but if you want to
* know, let me know and I can walk you through it.
*/

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
        glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
                glfwSetWindowShouldClose(window, true);
        }
}

float windowWidth = 640;
float windowHeight = 480;

GLFWwindow* initWindow() {
        GLFWwindow* window;
        if (!glfwInit()) {
                return NULL;
        }

#ifdef __APPLE__
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

        window = glfwCreateWindow(windowWidth, windowHeight, "Lab 2", NULL, NULL);
        if (!window) {
                glfwTerminate();
                return NULL;
        }

        glfwMakeContextCurrent(window);
        glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
        if (!gladLoadGL()) {
                std::cout << "Failed to initialize OpenGL context" << std::endl;
                glfwTerminate();
                return NULL;
        }

        return window;
}

std::string shaderTypeName(GLenum shaderType) {
        switch (shaderType) {
        case GL_VERTEX_SHADER: return "VERTEX";
        case GL_FRAGMENT_SHADER: return "FRAGMENT";
        default: return "UNKNOWN";
        }
}

std::string readFile(const std::string& fileName) {
        std::ifstream stream(fileName);
        std::stringstream buffer;
        buffer << stream.rdbuf();

        std::string source = buffer.str();
        std::cout << "Source:" << std::endl;
        std::cout << source << std::endl;

        return source;
}

/** END OF CODE THAT YOU DON'T NEED TO WORRY ABOUT */

GLuint createShader(const std::string& fileName, GLenum shaderType) {
        std::string source = readFile(fileName);
        const char* src_ptr = source.c_str();

        /** YOU WILL ADD CODE STARTING HERE */
        // create the shader
        GLuint shader = glCreateShader(shaderType);
        glShaderSource(shader, 1, &src_ptr, NULL);
        glCompileShader(shader);
        /** END CODE HERE */

        // Perform some simple error handling on the shader
        int success;
        char infoLog[512];
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (!success) {
                glGetShaderInfoLog(shader, 512, NULL, infoLog);
                std::cerr << "ERROR::SHADER::" << shaderTypeName(shaderType)
                       << "::COMPILATION_FAILED\n"
                       << infoLog << std::endl;
        }

        return shader;
}

GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader) {
        /** YOU WILL ADD CODE STARTING HERE */

        // create the program
        unsigned int program = glCreateProgram();
        glAttachShader(program, vertexShader);    //attach the vertex and fragment shaders
        glAttachShader(program, fragmentShader);
        glLinkProgram(program);
        glUseProgram(program);

        /** END CODE HERE */

        // Perform some simple error handling
        int success;
        glGetProgramiv(program, GL_LINK_STATUS, &success);
        if (!success) {
                char infoLog[512];
                glGetProgramInfoLog(program, 512, NULL, infoLog);
                std::cerr << "ERROR::PROGRAM::COMPILATION_FAILED\n"
                       << infoLog << std::endl;
        }

        return program;
}

class Point {
        //Class for holding points, coordinates and colors
public:
        float x;
        float y;
        float r;
        float g;
        float b;

        Point(float xx, float yy, float rr, float gg, float bb) : x(xx), y(yy), r(rr), g(gg), b(bb) {
                r = r * 255;
                g = g * 255;
                b = b * 255;
        }
        Point() {

        }
        float dot(Point p) {
                float d_product = x*p.x + y*p.y;
                return d_product;
        }
        Point minus(Point p) {
                Point difference = Point(x - p.x, y - p.y, 1, 1, 1);
                return difference;
        }
        // function that normalizes the coodinates
        void convertCoordinates(float xin, float yin) {
                x = -1 + xin * (2 / windowWidth);
                y = 1 - yin * (2 / windowHeight);
        }
};

int main() {

        /** YOU WILL ADD DATA INITIALIZATION CODE STARTING HERE */

        /* PART1: ask the user for coordinates and colors, and convert to normalized
        * device coordinates */

        std::cout << "Enter 3 points (enter a point as x,y:r,g,b):" << std::endl;
        Point bounds[3];
        for (int i = 0; i < 3; i++) {
                //input vertices
                std::string temp;
                char sep;
                std::cin >> bounds[i].x >> sep >> bounds[i].y >> sep >> bounds[i].r >> sep >> bounds[i].g >> sep >> bounds[i].b;
                bounds[i].convertCoordinates(bounds[i].x, bounds[i].y);
        }
        GLFWwindow* window = initWindow();

        if (!window) {
                std::cout << "There was an error setting up the window" << std::endl;
                return 1;
        }
        // convert the triangle to an array of floats containing
        // normalized device coordinates and color components.
        // x, y, z, r, b, g for each vertex
        float triangle[] = {
                bounds[0].x, bounds[0].y, 0.0f, bounds[0].r, bounds[0].g, bounds[0].b,
                bounds[1].x, bounds[1].y, 0.0f, bounds[1].r, bounds[1].g, bounds[1].b,
                bounds[2].x, bounds[2].y, 0.0f, bounds[2].r, bounds[2].g, bounds[2].b

        };
        for (int i = 0; i<9; i++) {
                std::cout << triangle[i] << std::endl;
        }
        // create the vertex and fragment shaders
        GLuint vertexShader = createShader("vert.glsl", GL_VERTEX_SHADER);
        GLuint fragmentShader = createShader("frag.glsl", GL_FRAGMENT_SHADER);

        // create the shader program
        // YOU WILL HAVE TO ADD CODE TO THE createShaderProgram FUNCTION ABOVE
        GLuint shaderProgram = createShaderProgram(vertexShader, fragmentShader);
        // PART2: map the data

        // cleanup the vertex and fragment shaders
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);

        // create vertex and array buffer objects
                unsigned int VBO, VAO;
        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);

        // setup triangle
        // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
        glBindVertexArray(VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
        glEnableVertexAttribArray(1);
        // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
        // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
        glBindVertexArray(0);
        // setup the attribute pointer for the coordinates
        // setup the attribute pointer for the colors
        // both will use glVertexAttribPointer and glEnableVertexAttribArray;

        /** PART3: create the shader program */

        // create the shaders
        // YOU WILL HAVE TO ADD CODE TO THE createShader FUNCTION ABOVE
        // create the shader program
        // YOU WILL HAVE TO ADD CODE TO THE createShaderProgram FUNCTION ABOVE
        // cleanup the vertex and fragment shaders using glDeleteShader

        /** END INITIALIZATION CODE */

        while (!glfwWindowShouldClose(window)) {
                // you don't need to worry about processInput, all it does is listen
                // for the escape character and terminate when escape is pressed.
                processInput(window);

                /** YOU WILL ADD RENDERING CODE STARTING HERE */
                /** PART4: Implemting the rendering loop */

                // clear the screen with your favorite color using glClearColor
                glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
                glClear(GL_COLOR_BUFFER_BIT);
                // set the shader program using glUseProgram
                glUseProgram(shaderProgram);
                // bind the vertex array using glBindVertexArray
                glBindVertexArray(VAO); // seeing as we only have a single VAO there's no need to bind it every time, but we'll do so to keep things a bit more organized
                // draw the triangles using glDrawArrays
                glDrawArrays(GL_TRIANGLES, 0, 3);

                /** END RENDERING CODE */

                // Swap front and back buffers
                glfwSwapBuffers(window);
                glfwPollEvents();
        }

        glfwTerminate();
        return 0;
}
